use fizzbuzz::fizzbuzz;

macro_rules! acceptance_test {
    ($suite:ident, $($name:ident: $input:expr, $output:expr,)*) => {
        mod $suite {
            use super::*;
            $(
                #[test]
                fn $name() -> () {
                    let out = fizzbuzz($input);
                    assert_eq!($output, out);
                }
            )*
        }
    };
}

acceptance_test!(simple,
    one: 1, "1",
    two: 2, "2",
);

acceptance_test!(fizz_on_divisible_by_3,
    three: 3, "Fizz",
    six: 6, "Fizz",
    nine: 9, "Fizz",
);

acceptance_test!(other_larger_numbers,
    four: 4, "4",
    seven: 7, "7",
    twentysix: 26, "26",
);

acceptance_test!(buzz_on_divisible_by_5,
    five: 5, "Buzz",
    ten: 10, "Buzz",
);

acceptance_test!(fizzbuzz_on_divisible_by_3_and_5,
    fifteen: 15, "FizzBuzz",
    thirteen: 30, "FizzBuzz",
);
