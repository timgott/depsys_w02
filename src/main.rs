use fizzbuzz::fizzbuzz;
use text_io::read;

fn main() {
    print!("Enter a number: ");
    let n: u32 = read!();
    println!();

    for i in 1..n {
        println!("{}", fizzbuzz(i));
    }
}
