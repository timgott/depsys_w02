//! `fizzbuzz` — Fizzbuzz computation library

/// Compute the fizzbuzz string at number `n`:
/// If `n` is divisible by 3, returns "Fizz".
/// If `n` is divisible by 5, returns 5.
/// If `n` is divisible by both 3 and 5, returns "FizzBuzz".
/// Otherwise, returns string representing the number `n`
pub fn fizzbuzz(n: u32) -> String {
    if n % 3 == 0 {
        if n % 5 == 0 {
            "FizzBuzz".to_string()
        } else {
            "Fizz".to_string()
        }
    } else if n % 5 == 0 {
        "Buzz".to_string()
    } else {
        n.to_string()
    }
}
